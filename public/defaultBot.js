/**
 * @author Dmitry Cheva <dmitry.cheva@gmail.com>
 * @version 0.2.34b (17.10.2017)
 */

'use strict';

// global state
var v034_triggers = [];
var v034_time = +new Date();

/**
 *
 * @param data
 * @returns {{direction, velocity}}
 */
function getPlayerMove(data) {

    const sixthPartOfFieldWidth = data.settings.field.width / 6;
    const centerOfFieldX = data.settings.field.width / 2;
    const height = data.settings.field.height;

    var ball = data.ball;
    var currentPlayer = data.yourTeam.players[data.playerIndex];
    var players = data.yourTeam.players;
    var ballStopQ = getDistance(ball, currentPlayer) > sixthPartOfFieldWidth
        ? 1
        : ((sixthPartOfFieldWidth - getDistance(ball, currentPlayer)) / sixthPartOfFieldWidth);
    var ballStop = getBallStats(ball, data.settings, ballStopQ);
    var randomOffset = ball.settings.radius / 2 * Math.random() - ball.settings.radius / 4;
    var attackOffset = ball.settings.radius;
    var attackDirection = Math.atan2(ballStop.y - currentPlayer.y, ballStop.x - currentPlayer.x - attackOffset);
    var maxPlayerVelocity = data.settings.player.maxVelocity;

    // init
    if (ball.velocity === 0 && allPlayresVelocity(players) === 0) {
        // console.log(Date(v034_time), 'init');
        if (centerOfFieldX < currentPlayer.x) {
            // console.log('left side')
        } else {
            // console.log('right side')
        }
        v034_triggers.forEach(function (item, i, arr) {
            v034_triggers[i] = []
        });

    }
    // local init
    var v034_ctime = +new Date();

    // strike!
    if (ball.x === centerOfFieldX && ball.velocity === 0 && (data.playerIndex !== 1)) {
        return move(Math.atan2(ball.y - currentPlayer.y, ball.x - currentPlayer.x - attackOffset + (randomOffset*(data.playerIndex-1)), maxPlayerVelocity);
    }

    // default intercept
    if (ball.x < currentPlayer.x
        && (Math.cos(ball.direction < -0.2))
    ) {
        return getInterceptDirection(data, ballStop);
    }

    var defenderPoint = ballStop.x >= centerOfFieldX + 50
        ? sixthPartOfFieldWidth * 1.2 : 0;
    // ? ((centerOfFieldX + sixthPartOfFieldWidth * 2) : sixthPartOfFieldWidth) : 0;
    //     ? sixthPartOfFieldWidth * 2 : sixthPartOfFieldWidth) : 0;
    var teamSide = [
        sixthPartOfFieldWidth * 3
        , defenderPoint + 50
        , sixthPartOfFieldWidth * 2
    ];
    teamSide.forEach(function (item, i, arr) {
        // goalie/interceptor [1]
        if (i === 1) {
            if (ballStop.x >= defenderPoint + centerOfFieldX
                && !is_empty(v034_triggers[i])
                && !is_empty(v034_triggers[i].intercept)
                || ballStop.x <= defenderPoint
            ) {
                // console.log((aaa_ctime - v034_time) / 1000, 'Detriggered', i)
                v034_triggers[i] = [];
            }
            if (ballStop.x >= defenderPoint + centerOfFieldX // opponents side
            ) {
                // keep the gate
                if (is_empty(v034_triggers[i])
                    || !is_empty(v034_triggers[i].intercept)
                ) {
                    // console.log((aaa_ctime - v034_time) / 1000, 'Triggered position', i)
                    v034_triggers[i] = {position: {x: item}};
                }
            } else {
                if (!is_empty(v034_triggers[i])
                    && (is_empty(v034_triggers[i].intercept)
                        && !is_empty(v034_triggers[i].position))
                    || (getNearest(ballStop, data.yourTeam.players).nearest === currentPlayer)
                ) {
                    v034_triggers[i] = {intercept: true};
                }
            }
        }
        // midfielder/forward [0,2]
        else {
        }
    });

    // triggers processing
    var playerTrigger = v034_triggers[data.playerIndex];
    if (!is_empty(playerTrigger)) {
        // fixed position
        // if(data.playerIndex!==1) console.log('triggeredPosition', playerTrigger.position);
        if (!is_empty(playerTrigger.position)) {
            // go to position
            return triggeredPosition(playerTrigger, ball, currentPlayer, height);
        }
        if (!is_empty(playerTrigger.intercept)
            && ball.x > currentPlayer.x
        ) {
            // counterattack
            var direction = Math.atan2(ballStop.y - currentPlayer.y, ballStop.x - currentPlayer.x - attackOffset);
            var velocity = (Math.cos(currentPlayer.direction - direction) > 0.5)
                ? maxPlayerVelocity :
                (maxPlayerVelocity * (Math.cos(currentPlayer.direction - direction)));
            return move(direction, velocity);
        }
    }

    // default atttack
    if (getDistance(ball, currentPlayer) > sixthPartOfFieldWidth / 2) {
        attackDirection += (data.playerIndex - 1) / 3;
    }
    var moveTo = avoidCollision(data, attackDirection, maxPlayerVelocity);
    // orbital move
    if (Math.cos(moveTo.direction - currentPlayer.direction) < 0.5 && moveTo.velocity > 3) moveTo.velocity = moveTo.velocity - 1;
    return move(moveTo.direction, moveTo.velocity);
}

function allPlayresVelocity(players) {
    var sum = 0;
    players.forEach(function (player, i, all) {
        sum += getPlayerVelocity(player);
    });
    return sum;
}

function getPlayerVelocity(player) {
    return player.velocity;
}

/**
 *
 * @param playerTrigger
 * @param ball
 * @param currentPlayer
 * @param height
 * @returns {{direction: *, velocity: *}}
 */
function triggeredPosition(playerTrigger, ball, currentPlayer, height) {
    var pX = playerTrigger.position.x;
    var pYlposCenter = height / 2;
    var pYlimitRight = pYlposCenter - pYlposCenter / 3;
    var pYlimitLeft = pYlposCenter + pYlposCenter / 3;
    var pYposRight = pYlposCenter - pYlposCenter / 2;
    var pYlposLeft = pYlposCenter + pYlposCenter / 2;
    if (velocity < 0.5) {
        pX = ball.x;
        pY = ball.y;
    }
    var pY = (ball.y < pYlimitRight ? pYposRight : (ball.y > pYlimitLeft ? pYlposLeft : pYlposCenter));
    var distance = getDistance({x: pX, y: pY}, currentPlayer);
    var velocity = distance > 80 ? 6 : 6 * (distance / 80);
    var direction = Math.atan2(pY - currentPlayer.y, pX - currentPlayer.x);
    // @todo отнимать косинус направления игрока от косуинуса направления на мяч
    return move(direction, velocity);
}

/**
 *
 * @param arr
 * @returns {boolean}
 */
function is_empty(arr) {
    if (typeof arr === 'undefined' || arr.length === 0) {
        return true;
    }
}

/**
 *
 * @param data
 * @param ballStop
 * @returns {{direction: number, velocity: number}}
 */
function getInterceptDirection(data, ballStop) {
    var currentPlayer = data.yourTeam.players[data.playerIndex];
    var ballMargin = data.ball.settings.radius * 4;
    var interceptMargin = (ballStop.y < currentPlayer.y) ? ballMargin : -ballMargin;
    var direction = Math.atan2((ballStop.y + interceptMargin - currentPlayer.y), (ballStop.x - currentPlayer.x - ballMargin));
    var moveTo = avoidCollision(data, direction, data.settings.player.maxVelocity);
    return move(moveTo.direction, moveTo.velocity);
}

/**
 *
 * @param data
 * @param direction
 * @param velocity
 * @returns {*}
 */
function avoidCollision(data, direction, velocity) {
    // avoid collision
    var currentPlayer = data.yourTeam.players[data.playerIndex];
    var nearestTeamPlayer = getNearest(currentPlayer, data.yourTeam.players).nearest;
    if ((getDistance(currentPlayer, nearestTeamPlayer) < data.ball.settings.radius * 4)
        && (getDistance(currentPlayer, data.ball) > getDistance(nearestTeamPlayer, data.ball))) {
        // direction = getDirectionTo(currentPlayer, nearestTeamPlayer) - Math.PI;
        velocity = velocity - 1;
    }
    return {direction, velocity};
}

/**
 *
 * @param from
 * @param toArray
 * @returns {{}}
 */
function getNearest(from, toArray) {
    var nearest = {};
    var nearestId;
    var minDistance = 1000;
    var distance;
    for (var i in toArray) {
        if (toArray[i] !== from) {
            distance = getDistance(from, toArray[i]);
            if (distance < minDistance) {
                minDistance = distance;
                nearest = toArray[i];
                nearestId = i;
            }
        }
    }
    return {nearest, nearestId, minDistance};
}

/**
 *
 * @param direction
 * @param velocity
 * @returns {{direction: *, velocity: *}}
 */
function move(direction, velocity) {
    // default atttack
    return {
        direction: direction, velocity: velocity
    };
}

/**
 *
 * @param point1
 * @param point2
 * @returns {*}
 */
function getDistance(point1, point2) {
    return Math.hypot(point1.x - point2.x, point1.y - point2.y);
}

/**
 *
 * @param ball
 * @param gameSettings
 * @returns {{stopTime, stopDistance: number, x: *, y: number}}
 */
function getBallStats(ball, gameSettings, q) {
    var stopTime = getStopTime(ball);
    var stopDistance = ball.velocity * stopTime
        - ball.settings.moveDeceleration * stopTime * stopTime / 2;
    stopDistance = stopDistance * q;

    var x = ball.x + stopDistance * Math.cos(ball.direction);
    var y = Math.abs(ball.y + stopDistance * Math.sin(ball.direction));

    // check the reflection from field side
    if (y > gameSettings.field.height) y = 2 * gameSettings.field.height - y;

    return {stopTime, stopDistance, x, y};
}

/**
 *
 * @param ball
 * @returns {number}
 */
function getStopTime(ball) {
    return ball.velocity / ball.settings.moveDeceleration;
}

/**
 *
 * @param startPoint
 * @param endPoint
 * @returns {number}
 */
function getDirectionTo(startPoint, endPoint) {
    var direction = Math.atan2(endPoint.y - startPoint.y, endPoint.x - startPoint.x);
    if (direction > Math.PI) direction -= Math.PI;
    return direction;
}

/**
 * export
 */
onmessage = (e) =>
postMessage(getPlayerMove(e.data));