'use strict';

var zzz_startPass;
var zzz_rand = 0;
var zzz_init = true;
var zzz_strategy = 4;
var zzz_strategyLast;
var zzz_strategyStats = [];

function getPlayerMove(data) {
    var startPoint, stopPoint;
    var currentPlayer = data.yourTeam.players[data.playerIndex];
    const sixthPartOfFieldWidth = data.settings.field.width / 6;
    const centerOfFieldY = data.settings.field.height / 2;
    const centerOfFieldX = data.settings.field.width / 2;
    var ball = data.ball;
    var ballStop = getBallStats(ball, data.settings);
    const ballRadius = ball.settings.radius;
    var velocity = data.settings.player.maxVelocity;

    startPoint = {
        x: (typeof currentPlayer !== 'undefined') ? currentPlayer.x : 0,
        y: (typeof currentPlayer !== 'undefined') ? currentPlayer.y : centerOfFieldY
    };

    // zzz_init
    if (data.playerIndex === 0 && ball.velocity === 0 && currentPlayer.velocity === 0 && zzz_init) {
        zzz_rand = Math.random() * ballRadius / 2;
        zzz_startPass = [0, 2][Math.floor(Math.random() * 2)];
        zzz_strategy = checkzzz_strategy(data);
        zzz_init = false;
    } else {
        // zzz_rand = 0;
        zzz_init = true;
    }

    // console.log('default: ' + zzz_strategy);
    // стартовая стратегия
    if (ball.velocity === 0) {
        if ((zzz_strategy === 0) && zzz_startPass === data.playerIndex) {
            // console.log('> выход');
            return {
                direction: 0,
                velocity: currentPlayer.velocity + data.settings.player.maxVelocityIncrement
            }
        } else {
            // Розыгрыш
            return {
                direction: getDirectionTo({x: currentPlayer.x, y: currentPlayer.y + zzz_rand}, ball),
                velocity: currentPlayer.velocity + data.settings.player.maxVelocityIncrement
            }
        }
    }
    // в полузащите
    if ((zzz_strategy === 3 || zzz_strategy === 1 || zzz_strategy === 0)  && data.playerIndex === 1) {
        if (ball.x > (sixthPartOfFieldWidth * 4)) {
            // console.log('> полузащита')
            return {
                direction: getDirectionTo({x: currentPlayer.x, y: currentPlayer.y}, {
                    x: sixthPartOfFieldWidth * 2,
                    y: ball.y
                }),
                velocity: Math.abs(ball.y - currentPlayer.y) / ballRadius
            }
        }
    }
    // в глубокой полузащите
    if ((zzz_strategy === 2)  && data.playerIndex !== 0) {
        if (ball.x > (sixthPartOfFieldWidth * (2 + data.playerIndex))) { // 3,4
            // console.log('> полузащита')
            return {
                direction: getDirectionTo({x: currentPlayer.x, y: currentPlayer.y}, {
                    x: sixthPartOfFieldWidth * 1 + data.playerIndex,
                    y: ball.y
                }),
                velocity: Math.abs(ball.y - currentPlayer.y) / ballRadius
            }
        }
    }
    // в нападении
    if ((zzz_strategy === 3 ) && data.playerIndex === zzz_startPass && ball.x < (sixthPartOfFieldWidth * 4)) {
        // console.log('> нападение')
        var startATtack = true;
        for (var i in data.yourTeam.players) {
            if ((data.playerIndex !== i && data.yourTeam.players[i].x > ball.x)) {
                startATtack = false;
            }
        }
        if (startATtack) {
            // console.log('бросок');
            return {
                direction: getDirectionTo({x: currentPlayer.x, y: currentPlayer.y}, {
                    x: sixthPartOfFieldWidth * 4,
                    y: ball.y
                }),
                velocity: Math.abs(ball.y - currentPlayer.y) / ballRadius
            }

        }
    }

    // Игра
    // Если мяч ближе к воротам - оббежать, потом развернуться и в атаку
    // console.log('> игра')
    var myX, myY;
    var varDistance = ballRadius * (2 + // 10 * ([2 + 4*[0..1]) // скорость мяча
        (4 * ((getDistance(ball, currentPlayer) > 60) ? 1 : (getDistance(ball, currentPlayer) / 60)))); // расстоянме до мяча
    varDistance = -ballRadius * 2 + ball.x < 100 ? varDistance * ball.x / 100 : varDistance; // дистанция до варот важна
    if ((ball.x - varDistance) < currentPlayer.x // 2..6 радиусов
        && (1.8 < ball.direction < 4.5) // летит к нам
        && ball.x < sixthPartOfFieldWidth * 3) { // наши 2/3
        // отбор
        myX = ball.x - varDistance;
        myY = (ball.y > currentPlayer.y) ? ball.y - 40 : ball.y + 40;
        // if (zzz_startPass === data.playerIndex) console.log('отбор');
    } else {
        // атака
        myX = ballStop.x - ballRadius * 2;
        myY = ballStop.y;
        // if (zzz_startPass === data.playerIndex) console.log('атака');
    }

    stopPoint = {
        x: myX,
        y: myY + zzz_rand
    };

    return {
        direction: getDirectionTo(startPoint, stopPoint),
        velocity: velocity
    };
}

function checkzzz_strategy(data) {
    // первый вызов
    if (typeof zzz_strategyStats[zzz_strategy] === 'undefined') {
        zzz_strategy = 4;
        for(i=0;i<5;i++) {
            zzz_strategyStats[i] = {
                q : 1+(i/1000000000000000),
                data: data
            };
        }
        console.log('Стратегия: ' + zzz_strategy);
    }
    // каждые 5 пропущенных
    var lastOpGoals = (typeof zzz_strategyLast ===  'undefined') ? 0 : zzz_strategyLast.data.opponentTeam.goals
    var lastMyGoals = (typeof zzz_strategyLast ===  'undefined') ? 0 : zzz_strategyLast.data.yourTeam.goals
    if (data.opponentTeam.goals - lastOpGoals < 3) return zzz_strategy;
    zzz_strategyLast = zzz_strategyStats[zzz_strategy];
    zzz_strategyStats[zzz_strategy].data = data;
    // считаем голы
    zzz_strategyStats[zzz_strategy].q = (zzz_strategyStats[zzz_strategy].q + (data.yourTeam.goals - lastMyGoals)
        / (data.opponentTeam.goals - lastOpGoals)) / 2;
    console.log('Текущий Q: ' + zzz_strategyStats[zzz_strategy].q);
    var myMax = 0;
    for(var i=0;i<5;i++) {
        if (zzz_strategyStats[i].q > myMax) {
            myMax = zzz_strategyStats[i].q;
            zzz_strategy = i;
        }
    }
    console.log('Максима Q: ' + myMax);
    console.log('Стратегия: ' + zzz_strategy);
    return zzz_strategy;
}

function getDirectionTo(startPoint, endPoint) {
    return Math.atan2(endPoint.y - startPoint.y, endPoint.x - startPoint.x);
}

function getDistance(point1, point2) {
    return Math.hypot(point1.x - point2.x, point1.y - point2.y);
}

function getBallStats(ball, gameSettings) {
    var stopTime = getStopTime(ball);
    var stopDistance = ball.velocity * stopTime
        - ball.settings.moveDeceleration * (stopTime + 1) * stopTime / 2;

    var x = ball.x + stopDistance * Math.cos(ball.direction);
    var y = Math.abs(ball.y + stopDistance * Math.sin(ball.direction));

    // check the reflection from field side
    if (y > gameSettings.field.height) y = 2 * gameSettings.field.height - y;

    return {stopTime, stopDistance, x, y};
}

function getStopTime(ball) {
    return ball.velocity / ball.settings.moveDeceleration;
}

onmessage = (e) =>
postMessage(getPlayerMove(e.data));
