/**
 * @author Dmitry Cheva <dmitry.cheva@gmail.com>
 * @version 0.2.1 (08.10.2017)
 */

'use strict';

// global state
var xxx_trigger = [];
var xxx_time = +new Date();

/**
 *
 * @param data
 * @returns {{direction, velocity}}
 */
function getPlayerMove(data) {
    var currentPlayer = data.yourTeam.players[data.playerIndex];
    var players = data.yourTeam.players;
    var ball = data.ball;
    var ballStop = getBallStats(ball, data.settings);
    var ballMargin = ball.settings.radius * 4;
    var randomOffset = ball.settings.radius / 2 * Math.random() - ball.settings.radius / 4;
    var attackOffset = ball.settings.radius;
    var attackDirection = Math.atan2(ballStop.y - currentPlayer.y, ballStop.x - currentPlayer.x - attackOffset);
    var maxVelocity = data.settings.player.maxVelocity;
    var ctime = +new Date();

    const sixthPartOfFieldWidth = data.settings.field.width / 6;
    const eightPartOfFieldWidth = data.settings.field.width / 8;
    const halfPartOfFieldWidth = data.settings.field.width / 2;
    const centerOfFieldY = data.settings.field.height / 2;
    const centerOfFieldX = data.settings.field.width / 2;

    // init

    // strike!
    if (ball.velocity == 0 && (data.playerIndex !== 1)) {
        return move(Math.atan2(ball.y - currentPlayer.y, ball.x - currentPlayer.x - attackOffset + randomOffset), maxVelocity);
    }

    // default defend
    if (ball.x < currentPlayer.x && 2 < ball.direction < 4.2) { // 120..240 degrees
        // @todo attacker must stay ahead
        return getDefendDirection(data, ballStop);
    }

    // attacker [0]
    // if [1,2] are on team side of ball - forward for 1/6 from ball and wait
    if (data.playerIndex === 0) {

    }

    // goalie/defender [1]
    // clear trigger (counterattack is over)
    if(!is_empty(xxx_trigger[1]) && !is_empty(xxx_trigger[1].attack)
            && ball.x >= centerOfFieldX){
        xxx_trigger[1]=[];
        // console.log((ctime-xxx_time)/1000,'Detriggered', 1)
    }
    if (ball.x >= centerOfFieldX // opponents side
        || (players[0].x < ball.x && players[2].x < ball.x)) {
        // hold position
        if (is_empty(xxx_trigger[1])) {
            xxx_trigger[1] = {position: {x: 75}}; // XY object, size, from distance, to distance
            // console.log((ctime-xxx_time)/1000 ,'Triggered goalie', 1)
        }
    } else if(!is_empty(xxx_trigger[1]) && is_empty(xxx_trigger[1].attack)){
        // attack
        xxx_trigger[1] = {attack:true};
        // console.log((ctime-xxx_time)/1000,'Triggered counterattack', 1)
    }

    // triggers processing
    var t = xxx_trigger[data.playerIndex];
    // fixed position
    if (!is_empty(t) && !is_empty(t.position)) {
        // go to position
        return triggeredPosition(t, ball, currentPlayer);
    }
    if (!is_empty(t) && !is_empty(t.attack)) {
        // go to position
        return move(Math.atan2(ballStop.y - currentPlayer.y, ballStop.x - currentPlayer.x - attackOffset),maxVelocity/1.5);
    }

    // free to play [2]
    // default atttack
    if (getDistance(ball, currentPlayer) > sixthPartOfFieldWidth / 2) {
        attackDirection += (data.playerIndex - 1) / 3;
    }
    return move(attackDirection, maxVelocity);
}

/**
 *
 * @param t
 * @param ball
 * @param currentPlayer
 * @returns {{direction: *, velocity: *}}
 */
function triggeredPosition(t, ball, currentPlayer){
    var pX = t.position.x;
    var pY = ball.y/1.5+80;
    var distance = getDistance({x: pX, y: pY}, currentPlayer);
    var direction = Math.atan2(pY - currentPlayer.y, pX - currentPlayer.x);
    var velocity = distance > 40 ? 6 : 6 *(distance / 40);
    return move(direction, velocity);
}

/**
 *
 * @param arr
 * @returns {boolean}
 */
function is_empty(arr) {
    if (typeof arr === 'undefined' || arr.length === 0) {
        return true;
    }
}

/**
 *
 * @param data
 * @param ballStop
 * @returns {{direction: number, velocity: number}}
 */
function getDefendDirection(data, ballStop) {
    var currentPlayer = data.yourTeam.players[data.playerIndex];
    var ballMargin = data.ball.settings.radius * 4;
    var defendOffset = ballStop.y < currentPlayer.y ? ballMargin : -ballMargin;
    var offsetY = ballStop.y + defendOffset;
    var distance = getDistance(currentPlayer, {x:ballStop.x,y:offsetY});
    var direction = Math.atan2((offsetY - currentPlayer.y), (ballStop.x - currentPlayer.x - ballMargin));
    return move(direction, data.settings.player.maxVelocity);
}

/**
 *
 * @param direction
 * @param velocity
 * @returns {{direction: *, velocity: *}}
 */
function move(direction, velocity) {
    // default atttack
    return {direction: direction,velocity: velocity
    };
}

/**
 *
 * @param point1
 * @param point2
 * @returns {*}
 */
function getDistance(point1, point2) {
    return Math.hypot(point1.x - point2.x, point1.y - point2.y);
}

/**
 *
 * @param ball
 * @param gameSettings
 * @returns {{stopTime, stopDistance: number, x: *, y: number}}
 */
function getBallStats(ball, gameSettings) {
    var stopTime = getStopTime(ball);
    var stopDistance = ball.velocity * stopTime
        - ball.settings.moveDeceleration * (stopTime + 1) * stopTime / 2;

    var x = ball.x + stopDistance * Math.cos(ball.direction);
    var y = Math.abs(ball.y + stopDistance * Math.sin(ball.direction));

    // check the reflection from field side
    if (y > gameSettings.field.height) y = 2 * gameSettings.field.height - y;

    return {stopTime, stopDistance, x, y};
}

/**
 *
 * @param ball
 * @returns {number}
 */
function getStopTime(ball) {
    return ball.velocity / ball.settings.moveDeceleration;
}

/**
 * export
 */
onmessage = (e) =>
postMessage(getPlayerMove(e.data));