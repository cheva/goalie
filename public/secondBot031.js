/**
 * @author Dmitry Cheva <dmitry.cheva@gmail.com>
 * @version 0.2.31 (09.10.2017) 67/1/7 135 popints, got 2nd place
 */

'use strict';

// global state
var v023_trigger = [];
var v023_time = +new Date();

/**
 *
 * @param data
 * @returns {{direction, velocity}}
 */
function getPlayerMove(data) {
    var ball = data.ball;
    var currentPlayer = data.yourTeam.players[data.playerIndex];
    var players = data.yourTeam.players;
    var ballStopQ = getDistance(ball, currentPlayer) > 100 ? 1 : ((100 - getDistance(ball, currentPlayer)) / 100);
    var ballStop = getBallStats(ball, data.settings, ballStopQ);
    var ballMargin = ball.settings.radius * 4;
    var randomOffset = ball.settings.radius / 2 * Math.random() - ball.settings.radius / 4;
    var attackOffset = ball.settings.radius;
    var attackDirection = Math.atan2(ballStop.y - currentPlayer.y, ballStop.x - currentPlayer.x - attackOffset);
    var maxVelocity = data.settings.player.maxVelocity;

    const sixthPartOfFieldWidth = data.settings.field.width / 6;
    const eightPartOfFieldWidth = data.settings.field.width / 8;
    const halfPartOfFieldWidth = data.settings.field.width / 2;
    const centerOfFieldY = data.settings.field.height / 2;
    const centerOfFieldX = data.settings.field.width / 2;

    // init
    var aaa_ctime = +new Date();

    // strike!
    if (ball.velocity === 0 && (data.playerIndex !== 1)) {
        return move(Math.atan2(ball.y - currentPlayer.y, ball.x - currentPlayer.x - attackOffset + randomOffset), maxVelocity);
    }

    // default intercept
    if (ball.x < currentPlayer.x && 2 < ball.direction < 4.2) { // 120..240 degrees
        // @todo attacker must stay ahead
        return getInterceptDirection(data, ballStop);
    }

    var teamSide = [sixthPartOfFieldWidth * 3, 75, sixthPartOfFieldWidth * 2];
    teamSide.forEach(function (item, i, arr) {
        // goalie/interceptor [1]
        if (i === 1) {
            if (!is_empty(v023_trigger[i]) && !is_empty(v023_trigger[i].attack)
                && ball.x >= centerOfFieldX) {
                //console.log((aaa_ctime - v023_time) / 1000, 'Detriggered interceptor')
                v023_trigger[i] = [];
            }
            if (ball.x >= centerOfFieldX // opponents side
                || (players[0].x < ball.x && players[2].x < ball.x)) {
                // keep the gate
                if (is_empty(v023_trigger[i])) {
                    //console.log((aaa_ctime - v023_time) / 1000, 'Triggered goalie', i)
                    v023_trigger[i] = {position: {x: item}};
                }
            } else if (!is_empty(v023_trigger[i]) && is_empty(v023_trigger[i].attack)) {
                //console.log((aaa_ctime - v023_time) / 1000, 'Triggered interceptor', i)
                v023_trigger[i] = {attack: true};
            }
        }
        // midfielder/forward [0,2]
        else {
        }
    });

    // triggers processing
    var playerTrigger = v023_trigger[data.playerIndex];
    if (!is_empty(playerTrigger)) {
        // fixed position
        // if(data.playerIndex!==1) console.log('triggeredPosition', playerTrigger.position);
        if (!is_empty(playerTrigger.position)) {
            // go to position
            return triggeredPosition(playerTrigger, ball, currentPlayer);
        }
        if (!is_empty(playerTrigger.attack)) {
            // counterattack
            var distance = getDistance(currentPlayer, ballStop);
            return move(Math.atan2(ballStop.y - currentPlayer.y, ballStop.x - currentPlayer.x - attackOffset), maxVelocity);
        }
    }

    // default atttack
    if (getDistance(ball, currentPlayer) > sixthPartOfFieldWidth / 2) {
        attackDirection += (data.playerIndex - 1) / 3;
    }
    return move(avoidCollision(data, attackDirection), maxVelocity);
}

/**
 *
 * @param t
 * @param ball
 * @param currentPlayer
 * @returns {{direction: *, velocity: *}}
 */
function triggeredPosition(playerTrigger, ball, currentPlayer) {
    var pX = playerTrigger.position.x;
    // @todo 3 fixed position (in the middle of 4 quarters)
    var pY = ball.y / 1.5 + 80;
    var distance = getDistance({x: pX, y: pY}, currentPlayer);
    var direction = Math.atan2(pY - currentPlayer.y, pX - currentPlayer.x);
    var velocity = distance > 80 ? 6 : 6 * (distance / 80);
    // @todo Ð¾Ñ‚Ð½Ð¸Ð¼Ð°Ñ‚ÑŒ ÐºÐ¾ÑÐ¸Ð½ÑƒÑ Ð½Ð°Ð¿Ñ€Ð°Ð²Ð»ÐµÐ½Ð¸Ñ Ð¸Ð³Ñ€Ð¾ÐºÐ° Ð¾Ñ‚ ÐºÐ¾ÑÑƒÐ¸Ð½ÑƒÑÐ° Ð½Ð°Ð¿Ñ€Ð°Ð²Ð»ÐµÐ½Ð¸Ñ Ð½Ð° Ð¼ÑÑ‡
    if (pX > 100) console.log('>>>', direction, velocity);
    return move(direction, velocity);
}

/**
 *
 * @param arr
 * @returns {boolean}
 */
function is_empty(arr) {
    if (typeof arr === 'undefined' || arr.length === 0) {
        return true;
    }
}

/**
 *
 * @param data
 * @param ballStop
 * @returns {{direction: number, velocity: number}}
 */
function getInterceptDirection(data, ballStop) {
    var currentPlayer = data.yourTeam.players[data.playerIndex];
    var ballMargin = data.ball.settings.radius * 4;
    var interceptOffset = ballStop.y < currentPlayer.y ? ballMargin : -ballMargin;
    var offsetY = ballStop.y + interceptOffset;
    var direction = Math.atan2((offsetY - currentPlayer.y), (ballStop.x - currentPlayer.x - ballMargin));
    return move(avoidCollision(data, direction), data.settings.player.maxVelocity);
}

/**
 *
 * @param data
 * @param direction
 * @returns {*}
 */
function avoidCollision(data, direction) {
    // avoid collision
    var currentPlayer = data.yourTeam.players[data.playerIndex];
    var nearestTeamPlayer = getNearest(currentPlayer, data.yourTeam.players).nearest;
    if ((getDistance(currentPlayer, nearestTeamPlayer) < data.ball.settings.radius * 3)
        && (getDistance(currentPlayer, data.ball) > getDistance(nearestTeamPlayer, data.ball))) {
        direction = getDirectionTo(currentPlayer, nearestTeamPlayer) - Math.PI;
    }
    return direction;
}

/**
 *
 * @param from
 * @param toArray
 * @returns {{}}
 */
function getNearest(from, toArray) {
    var nearest = {};
    var nearestId;
    var minDistance = 1000;
    var distance;
    for (var i in toArray) {
        if (toArray[i] !== from) {
            distance = getDistance(from, toArray[i]);
            if (distance < minDistance) {
                minDistance = distance;
                nearest = toArray[i];
                nearestId = i;
            }
        }
    }
    return {nearest, nearestId, minDistance};
}

/**
 *
 * @param direction
 * @param velocity
 * @returns {{direction: *, velocity: *}}
 */
function move(direction, velocity) {
    // default atttack
    return {
        direction: direction, velocity: velocity
    };
}

/**
 *
 * @param point1
 * @param point2
 * @returns {*}
 */
function getDistance(point1, point2) {
    return Math.hypot(point1.x - point2.x, point1.y - point2.y);
}

/**
 *
 * @param ball
 * @param gameSettings
 * @returns {{stopTime, stopDistance: number, x: *, y: number}}
 */
function getBallStats(ball, gameSettings, q) {
    var stopTime = getStopTime(ball);
    var stopDistance = ball.velocity * stopTime
        - ball.settings.moveDeceleration * (stopTime + 1) * stopTime / 2;
    stopDistance = stopDistance * q;

    var x = ball.x + stopDistance * Math.cos(ball.direction);
    var y = Math.abs(ball.y + stopDistance * Math.sin(ball.direction));

    // check the reflection from field side
    if (y > gameSettings.field.height) y = 2 * gameSettings.field.height - y;

    return {stopTime, stopDistance, x, y};
}

/**
 *
 * @param ball
 * @returns {number}
 */
function getStopTime(ball) {
    return ball.velocity / ball.settings.moveDeceleration;
}

/**
 *
 * @param startPoint
 * @param endPoint
 * @returns {number}
 */
function getDirectionTo(startPoint, endPoint) {
    return Math.atan2(endPoint.y - startPoint.y, endPoint.x - startPoint.x);
}

/**
 * export
 */
onmessage = (e) =>
postMessage(getPlayerMove(e.data));